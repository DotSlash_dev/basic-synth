#include <cmath>

#include "Note.h"

using namespace note;

Note::Note(int midi_value)
{
  _midi_value = midi_value;
  _note = midi_value % 12;
  _octave = midi_value / 12;
  calcFrequency();
}

Note::Note(Label label, Alteration alt, int octave)
{
  _note = label + alt;
  _octave = octave;
  _midi_value = 12 * _octave + _note;
  calcFrequency();
}

Note::~Note() { }

void Note::calcFrequency()
{
  _freq = 27.5f * pow(2, _octave + (_note + 3) / 12.0f);
}

int Note::midiValue()
{
  return _midi_value;
}

float Note::frequency()
{
  return _freq;
}
