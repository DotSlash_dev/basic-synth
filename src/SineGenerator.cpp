#include <cmath>

#include "SineGenerator.h"

namespace wave
{
  SineGenerator::SineGenerator(jack::AudioData* audio_data) :
    AbstractGenerator(audio_data)
  {}
  SineGenerator::~SineGenerator() {}

  void SineGenerator::generate()
  {
    float position = 0;
    float wave_length = static_cast<float>(_audio_data->sampleRate()) / _note->frequency();
    int buffer_length = static_cast<int>(wave_length * 100);
    for (unsigned int i = 0; i < wave_length; ++i)
    {
      if (position >= buffer_length)
        position -= buffer_length;
      _wave.add(sinf(2 * M_PI * position / buffer_length));
      position += 1;
    }
  }
}
