#include "AbstractGenerator.h"

namespace wave
{
  AbstractGenerator::AbstractGenerator(jack::AudioData* audio_data)
  {
    _audio_data = audio_data;
  }

  AbstractGenerator::~AbstractGenerator() {}

  void AbstractGenerator::note(note::Note* note) { _note = note; }
  Wave& AbstractGenerator::wave() { return _wave; }
}
