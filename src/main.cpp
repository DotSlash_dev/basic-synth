#include <iostream>
#include <vector>
#include <string>
#include <cmath>
#include <unistd.h>

#include "Jack.h"
#include "Note.h"

void fillBuffers(jack::AudioData& jack, std::vector<jack::Sample*>& ports)
{
  std::vector<jack::Sample*>::iterator it;
  float* audio_buffer = jack.buffer();

  for (unsigned int i = 0; i < jack.bufferSize(); ++i)
  {
    for (it = ports.begin(); it != ports.end(); ++it)
      (*it)[i] = audio_buffer[i];
  }
}

int processCallback(unsigned int nframes, void* data)
{
  jack::AudioData* jack = static_cast<jack::AudioData*>(data);
  std::vector<jack::Sample*> ports;
  ports.push_back(static_cast<jack::Sample*>(jack->getPortBuffer("Left", nframes)));
  ports.push_back(static_cast<jack::Sample*>(jack->getPortBuffer("Right", nframes)));
  fillBuffers(*jack, ports);
  ports.clear();
  return 0;
}

int main()
{
  jack::Engine client("Basic synth");

  if (!client.isReady())
    return 1;
  if (!client.registerPort("Left"))
    return 2;
  if (!client.registerPort("Right"))
    return 2;
  client.processCallback(processCallback);
  client.start();
  getchar();
  client.stop();
  client.unregisterAllPorts();
  return 0;
}
