#include <iostream>

#include "Jack.h"

namespace jack
{
  Engine::Engine(const std::string& name)
  {
    _name = name;
    _client = jack_client_open(_name.c_str(), JackSessionID, &_status, 0);
    _data = new AudioData(_client);
  }

  Engine::~Engine()
  {
    delete _data;
    jack_client_close(_client);
  }

  bool Engine::isReady() { return _status == 0; }

  bool Engine::registerPort(const std::string& name)
  {
    Port* port;
    port = jack_port_register(_client, name.c_str(), JACK_DEFAULT_AUDIO_TYPE, JackPortIsOutput, 0);
    if (port == NULL)
      return false;
    _data->addPort(name, port);
    return true;
  }

  void Engine::unregisterPort(const std::string& name)
  {
    Port* port = _data->removePort(name);
    jack_port_unregister(_client, port);
  }

  void Engine::unregisterAllPorts()
  {
    std::map<std::string, Port*> ports = _data->ports();
    std::map<std::string, Port*>::iterator it;
    for (it = ports.begin(); it != ports.end(); ++it)
    {
      jack_port_unregister(_client, it->second);
      _data->removePort(it->first);
    }
  }

  bool Engine::processCallback(int (*callback)(unsigned int, void*))
  {
    int err;
    err = jack_set_process_callback(_client, callback, static_cast<void*>(_data));
    return (err == 0);
  }

  bool Engine::start() { return jack_activate(_client) == 0; }
  bool Engine::stop() { return jack_deactivate(_client) == 0; }
}
