#include "Jack.h"

namespace jack
{
  AudioData::AudioData(jack_client_t* jack)
  {
    _sample_rate = jack_get_sample_rate(jack);
    _buffer_size = jack_get_buffer_size(jack);
    _buffer = new float[_buffer_size];
  }

  AudioData::~AudioData()
  {
    delete[] _buffer;
    _ports.clear();
  }

  std::map<std::string, Port*> AudioData::ports() { return _ports; }
  unsigned int AudioData::sampleRate() { return _sample_rate; }
  float* AudioData::buffer() { return _buffer; }
  unsigned int AudioData::bufferSize() { return _buffer_size; }

  void* AudioData::getPortBuffer(const std::string& name, unsigned int nframes)
  {
    std::map<std::string, Port*>::iterator it;
    it = _ports.find(name);
    if (it != _ports.end())
      return (jack_port_get_buffer(it->second, nframes));
    return NULL;
  }

  int AudioData::countPorts()
  {
    return _ports.size();
  }
  
  void AudioData::addPort(const std::string& name, Port* port)
  {
    _ports[name] = port;
  }

  Port* AudioData::removePort(const std::string& name)
  {
    Port* port = NULL;
    std::map<std::string, Port*>::iterator it;
    it = _ports.find(name);
    if (it != _ports.end())
    {
      port = it->second;
      _ports.erase(it);
    }
    return port;
  }
}
