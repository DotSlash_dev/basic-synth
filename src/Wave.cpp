#include "AbstractGenerator.h"

namespace wave
{
  void Wave::add(float value) { wave.push_back(value); }
  void Wave::clear() { wave.clear(); }
}
