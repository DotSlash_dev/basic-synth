##
## Makefile for basicsynth in /home/DotSlash/perso/c++/basicsynth
## 
## Made by kevin fontaine
## Login   <fontai_k@epitech.net>
## 
## Started on  Tue Mar 13 21:02:31 2012 kevin fontaine
## Last update Mon Feb 03 16:16:41 2014 kevin fontaine
##

NAME	= basicsynth
HDRDIR	= hdr
SRCDIR	= src
OBJDIR	= obj

SRC	= $(wildcard $(SRCDIR)/*.cpp)
OBJ	= $(addprefix $(OBJDIR)/,$(notdir $(SRC:.cpp=.o)))

CXX	= g++ -std=c++11
IFLAGS	= -I$(HDRDIR)
DFLAGS	=
LDFLAGS	= -ljack -lasound -pthread
CXXFLAGS= -W -Wall -Werror $(IFLAGS) $(DFLAGS) -g3

WHT	= \033[0m
RED	= \033[91m
GRN	= \033[92m
YLW	= \033[93m
BLU	= \033[94m

RM	= rm -f
CP	= cp -v
MKDIR	= mkdir -vp

$(NAME)	: $(OBJ)
	@echo -e "Linking $(GRN)$(NAME)$(WHT) with $(YLW)$(OBJ)$(WHT) and libraries $(BLU)$(LDFLAGS)$(WHT)"
	@$(CXX) -o $(NAME) $(OBJ) $(LDFLAGS)

$(OBJDIR)/%.o	: $(SRCDIR)/%.cpp
	@echo -e "Compiling $(GRN)$<$(WHT) into $(YLW)$@$(WHT) with options $(BLU)$(CXXFLAGS)$(WHT)"
	@$(CXX) -c -o $@ $< $(CXXFLAGS)

$(OBJDIR)/%.o	: $(SRCDIR)/%.cc
	@echo -e "Compiling $(GRN)$<$(WHT) into $(YLW)$@$(WHT) with options $(BLU)$(CXXFLAGS)$(WHT)"
	@$(CXX) -c -o $@ $< $(CXXFLAGS)

all	: $(NAME)

clean	:
	echo -e "Removing $(GRN)$(OBJ)$(WHT)"
	$(RM) $(OBJ)

fclean	: clean
	echo -e "Removing $(GRN)$(NAME)$(WHT)"
	$(RM) $(NAME)

re	: fclean all

init	:
	$(MKDIR) $(SRCDIR) $(HDRDIR) $(OBJDIR)

.PHONY	: all clean fclean re init
.SILENT	: all clean fclean re init
