#ifndef HH_ABSTRACT_GENERATOR
#define HH_ABSTRACT_GENERATOR

#include <vector>

#include "Jack.h"
#include "Note.h"

namespace wave
{
  struct Wave
  {
    std::vector<float> wave;
    int position;

    void add(float);
    void clear();

    Wave& operator+(const Wave&);
  };

  class AbstractGenerator
  {
    protected:
      AbstractGenerator(jack::AudioData*);

      jack::AudioData* _audio_data;
      Wave _wave;
      note::Note* _note;

    public:
      virtual ~AbstractGenerator();

      Wave& wave();
      void note(note::Note*);
      virtual void generate() = 0;
  };
}

#endif /* HH_ABSTRACT_GENERATOR */
