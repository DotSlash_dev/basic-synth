#ifndef HH_NOTE
#define HH_NOTE

namespace note
{
  enum Label
  {
    C = 0,
    D = 2,
    E = 4,
    F = 5,
    G = 7,
    A = 9,
    B = 11
  };
  enum Alteration { Flat = -1, Natural, Sharp };
  class Note
  {
    private:
      int _midi_value;
      int _note;
      int _octave;
      float _freq;

      void calcFrequency();

    public:
      Note(int);
      Note(Label, Alteration, int);
      ~Note();

      int midiValue();
      float frequency();
  };
}

#endif  /* HH_NOTE */
