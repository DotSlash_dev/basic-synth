#ifndef HH_JACK
#define HH_JACK

#include <map>
#include <string>
#include <jack/jack.h>

namespace jack
{
  typedef jack_port_t Port;
  typedef jack_default_audio_sample_t Sample;

  class AudioData
  {
    private:
      std::map<std::string, Port*> _ports;

      float* _buffer;
      unsigned int _buffer_size;
      jack_nframes_t _sample_rate;

    public:
      AudioData(jack_client_t*);
      AudioData(const AudioData&);
      AudioData& operator=(const AudioData&);
      ~AudioData();

      std::map<std::string, Port*> ports();
      unsigned int sampleRate();
      float* buffer();
      unsigned int bufferSize();

      void addPort(const std::string&, Port*);
      Port* removePort(const std::string&);
      void* getPortBuffer(const std::string&, unsigned int);
      int countPorts();
  };

  class Engine
  {
    private:
      std::string _name;

      jack_client_t* _client;
      jack_status_t _status;

      AudioData* _data;

      Engine(const Engine&);
      Engine& operator=(const Engine&);

    public:
      Engine(const std::string&);
      ~Engine();

      bool isReady();

      bool registerPort(const std::string&);
      void unregisterPort(const std::string&);
      void unregisterAllPorts();
      bool processCallback(int (*)(unsigned int, void*));

      bool start();
      bool stop();
  };
}

#endif /* HH_JACK */
