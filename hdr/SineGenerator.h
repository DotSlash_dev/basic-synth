#ifndef HH_SINE_GENERATOR
#define HH_SINE_GENERATOR

#include "AbstractGenerator.h"

namespace wave
{
  class SineGenerator : public AbstractGenerator
  {
    private:
      SineGenerator(const SineGenerator&);
      SineGenerator& operator=(const SineGenerator&);

    public:
      SineGenerator(jack::AudioData*);
      virtual ~SineGenerator();

      virtual void generate();
  };
}

#endif /* HH_SINE_GENERATOR */
